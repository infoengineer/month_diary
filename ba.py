from datetime import date


def replace_flag(flags, date_val, flag='1'):
    day_of_year = date_val.toordinal() - date(year, 1, 1).toordinal()
    flags_list = list(flags)
    flags_list[day_of_year] = flag
    return ''.join(flags_list) 


flags = '11111111100000110000011000001100000110000011000001102100110000021110001100000110000011000001100000110000011000001100000111100011110001100000110000011000001100000111000011000001100000110000011000001100000110000011000001100000110000011000001100000110000011000001100000110000011000001100000110000011000001100021110000011000001100000110000011000001100000110000011000001'
year = date.today().year

flags = replace_flag(flags=flags, date_val=date(year, 5, 4))
flags = replace_flag(flags=flags, date_val=date(year, 7, 8), flag='2')
flags = replace_flag(flags=flags, date_val=date(year, 10, 10), flag='2')
flags = replace_flag(flags=flags, date_val=date(year, 10, 11))

print(flags, file=open('flags_ba.txt', 'w'))
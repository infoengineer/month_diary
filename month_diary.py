import calendar
import locale
import requests
from datetime import date


from reportlab.platypus import SimpleDocTemplate, Paragraph, Table, TableStyle, PageBreak
from reportlab.lib import colors
from reportlab.lib.enums import TA_JUSTIFY, TA_RIGHT
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import mm
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont


pdfmetrics.registerFont(TTFont('DejaVuSans', 'DejaVuSans.ttf'))
pdfmetrics.registerFont(TTFont('DejaVuSans-Bold', 'DejaVuSans-Bold.ttf'))
pdfmetrics.registerFont(TTFont('DejaVuSans-Oblique', 'DejaVuSans-Oblique.ttf'))
pdfmetrics.registerFont(TTFont('DejaVuSans-BoldOblique', 'DejaVuSans-BoldOblique.ttf'))

styles = getSampleStyleSheet()
styles.add(ParagraphStyle(name='Justify', fontName='DejaVuSans', alignment=TA_JUSTIFY))
styles.add(ParagraphStyle(name='B-Justify', fontName='DejaVuSans-Bold', alignment=TA_JUSTIFY))
styles.add(ParagraphStyle(name='B-Right', fontName='DejaVuSans-Bold', alignment=TA_RIGHT))
styles.add(ParagraphStyle(name='BI-Right', fontName='DejaVuSans-BoldOblique', alignment=TA_RIGHT))

c = calendar.TextCalendar(calendar.MONDAY)
ru_month_names = ('Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь')
locale.setlocale(locale.LC_ALL, '')
day_abbr = tuple(calendar.day_abbr)
day_colors = ('black', 'red', 'grey')


def gen_month_sheet(cc, year, month):

    # r = requests.get('https://isdayoff.ru/api/getdata', params=dict(year=year, month=month, pre=1, sd=1))
    r = requests.get('http://infoengineer.ru/api/flags', params=dict(cc=cc, year=year, month=month))
    daysoff = tuple(r.json().get('flags'))

    elements, data = [], []

    l = [Paragraph(f'<font size=18>{year}</font>', styles['B-Justify']), 
         Paragraph(f'<font size=18>{ru_month_names[month-1]}</font>', styles['B-Right'])]
    
    data.append(l)

    t = Table(data, colWidths=[95*mm, 95*mm], rowHeights=30)
    t.setStyle(TableStyle([('VALIGN', (0, 0), (-1, -1), 'TOP'), ]))

    elements.append(t)

    data = []

    for i in (x for x in c.itermonthdays2(year, month) if not x[0] == 0):
        color = day_colors[int(daysoff[i[0] - 1])]

        l = [Paragraph(f'<font size=14 color={color}>{day_abbr[i[1]]}</font>', styles['Justify']), 
             Paragraph(f'<font size=14 color={color}>{i[0]}</font>', styles['BI-Right']), 
             '']

        data.append(l)

    t = Table(data, colWidths=[12*mm, 12*mm, 166*mm], rowHeights=24)
    t.setStyle(TableStyle([('LINEBELOW', (0, 0), (-1, -1), .5, colors.black),
              ('LINEAFTER', (1, 0), (1, -1), .5, colors.black),
              ('BOX',       (0, 0), (-1, -1), 1,   colors.black),
              ('VALIGN',    (0, 0), (-1, -1), 'TOP'), ]))

    elements.append(t)

    return elements


if __name__ == '__main__':

    now = date.today()
    year = now.year
    # month = now.month
    cc = 'ru'

    filename = f'{year}_{cc}.pdf'
    # filename = f'{year}_{month}_{cc}.pdf'
    
    doc = SimpleDocTemplate(filename, pagesize=A4, topMargin=10, bottomMargin=10)

    doc_elements = []

    for month in range(1, 13):
        doc_elements.extend(gen_month_sheet(cc, year, month))
        doc_elements.append(PageBreak())

    # doc_elements.extend(gen_month_sheet(cc, year, month))
    # doc_elements.append(PageBreak())

    doc.build(doc_elements)

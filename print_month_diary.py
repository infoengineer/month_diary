import click
from datetime import date
from month_diary import gen_month_sheet
from reportlab.platypus import SimpleDocTemplate, PageBreak
from reportlab.lib.pagesizes import A4


@click.command()
@click.argument('cc', default='ru', nargs=1, type=click.Choice(['ru', 'ba'], case_sensitive=False))
@click.argument('year', default=date.today().year, nargs=1, type=int)
@click.argument('month', default=0, nargs=1, type=int)
def print(year, month, cc):
    filename = f'{year}_{month}_{cc}.pdf'

    if month == 0:
        filename = f'{year}_{cc}.pdf'
        
    doc = SimpleDocTemplate(filename, pagesize=A4, topMargin=10, bottomMargin=10)
    doc_elements = []

    if month == 0:
        for month in range(1, 13):
            doc_elements.extend(gen_month_sheet(cc, year, month))
            doc_elements.append(PageBreak())
    else:
        doc_elements.extend(gen_month_sheet(cc, year, month))
        doc_elements.append(PageBreak())

    doc.build(doc_elements)


if __name__ == '__main__':
    print()
